# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email << user.username


#Friends !!!
42.times do |n|
  ActiveRecord::Base.transaction do
    username = "#{Faker::Hipster.sentence(1)}#{n}".gsub( /\W/, '' )[0..14]

    u = User.create!(
        username: username,
        password: "password",
        email: Faker::Internet.email
    )
    end
end

#add subs
subs = %w(OnePunchMan adventuretime)
subs_disaster = %w(earthquake cyclone flood fire wildfire transport_accident terrorism nuclear war epidemic famine)

descriptions = [
    "A sub to talk about and anticipate future One Punch chapters",
    "Subreddit dedicated to Cartoon Networks hit show, Adventure Time!"
]
descriptions_disaster = [
    "Official discussion about earthquake disasters",
    "Official discussion about cylone disasters",
    "Official discussion about flood disasters",
    "Official discussion about fire disasters",
    "Official discussion about wildfire disasters",
    "Official discussion about transport_accident",
    "Official discussion about terrorism disasters",
    "Official discussion about nuclear disasters",
    "Official discussion about war disasters",
    "Official discussion about epidemic disasters",
    "Official discussion about famine disasters"
]

subs_disaster.each_with_index do |sub, i|
  Sub.create!(
      title: "#{sub}",
      description: descriptions_disaster[i],
      moderator_id: User.all.sample.id,
      disaster: true
  )
end

subs.each_with_index do |sub, i|
  Sub.create!(
      title: "#{sub}",
      description: descriptions[i],
      moderator_id: User.all.sample.id
  )
end

#create giant hash to iterate over for posts
image_hashes = {
    :OnePunchMan => [
        {
            title: "OPM #84 - Chapter 56",
            url: "http://imgur.com/a/fhlGP/",
            content: "Here's latest OPM chapter"
        },

        {
            title: "One Punch Man Episode 9 Discussion"
        },

        {
            title: "When you rush home to watch the newest episode.",
            url: "https://media.giphy.com/media/11RfndQBVMW8U0/giphy.gif"
        }
    ],

    :adventuretime => [
        {
            title: "\"Checkmate\" and \"The Dark Cloud\" Episode Discussion Thread!"
        },

        {
            title: "More Me-Mow now!",
            url: "https://i.imgur.com/n37xvEC.png",
            content: "Adam Muto said at Comic-Con this year that Me-Mow will be returning this season!"
        },

        {
            title: "Piano Cover of Marceline's \"Everything Stays\" Song",
            url: "https://soundcloud.com/makoto-phoenix/everything-stays-marceline-adventure-time-cover"
        }
    ],
    :earthquake => [
        {
            title: "Earthquake San Francisco 2025",
            content: ""
        },
        {
            title: "Earthquake San Francisco 2025 - Victims",
            content: "List of known victims: Matt mackori, Jenna Andrew [...]"
        }

    ],
    :cyclone => [

    ],
    :flood => [

    ],
    :fire => [

    ],
    :wildfire => [

    ],
    :transport_accident => [

    ],
    :terrorism => [

    ],
    :nuclear => [

    ],
    :war => [

    ],
    :epidemic => [

    ],
    :famine => [

    ]
}

#create posts from image_hashes
Sub.all.each do |sub|
  id = sub.id
  title = sub.title.to_sym
  image_hashes[title].each do |hash|
    hash[:sub_id] = id
    hash[:submitter_id] = User.all.sample.id
    Post.create!(hash)
  end
end


#create some comments on all posts
Post.all.each do |post|
  3.times do
    c = Comment.create!(
        content: Faker::Hacker.say_something_smart,
        submitter_id: User.all.sample.id,
        post_id: post.id,
        parent_comment_id: nil
    )

    r = rand(10)

    if r <= 5
      r.times do
        r2 = rand(10)
        content = (r2 < 4) ? Faker::Hacker.verb : Faker::Hacker.say_something_smart

        c = Comment.create!(
            content: content,
            submitter_id: User.all.sample.id,
            post_id: post.id,
            parent_comment_id: c.id
        )

        if (r2 < 3)
          c2 = c
          r2.times do
            c2 = Comment.create!(
                content: Faker::Hacker.noun,
                submitter_id: User.all.sample.id,
                post_id: post.id,
                parent_comment_id: c2.id
            )
          end
        end
      end
    end
  end
end