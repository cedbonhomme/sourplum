class AddFriendlyidToSubs < ActiveRecord::Migration
  def change
    change_table(:subs) do |t|
      t.string :title,           :null => false, :default => ""
      t.string :slug
    end

    add_index :subs, :title,             :unique => true
    add_index :subs, :slug,                 :unique => true
  end
end
