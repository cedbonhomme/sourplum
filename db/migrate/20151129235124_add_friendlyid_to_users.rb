class AddFriendlyidToUsers < ActiveRecord::Migration
  def change
      change_table(:users) do |t|
          t.string :username,           :null => false, :default => ""
          t.string :slug
      end
      
      add_index :users, :username,             :unique => true
      add_index :users, :slug,                 :unique => true
  end
end
