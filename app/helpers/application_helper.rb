module ApplicationHelper
  def title(value)
    unless value.nil?
      @title = "#{value} | Sour Plum"
    end
  end

  def avatar_url(user)
    default_url = image_url("avatar.png")
    gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
    "http://gravatar.com/avatar/#{gravatar_id}.png?s=48&d=#{CGI.escape(default_url)}"
  end

  def notice
    flash[:notice]
  end

  def alert
    flash[:alert]
  end

end
