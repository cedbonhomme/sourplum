class SubsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :must_be_moderator, only: [:edit, :update, :destroy]

  # GET /subs
  # GET /subs.json
  def index
    @subs = Sub.without_disaster.all
  end

  # GET /subs/1
  # GET /subs/1.json
  def show
    @sub = Sub.includes(:moderator).friendly.find(params[:id])
  end

  # GET /subs/new
  def new
    @sub = Sub.new
  end

  # GET /subs/1/edit
  def edit
    @sub = Sub.friendly.find(params[:id])
  end

  # POST /subs
  # POST /subs.json
  def create
    @sub = current_user.owned_subs.new(sub_params)

    respond_to do |format|
      if @sub.save
        format.html { redirect_to @sub, notice: 'Sub was successfully created.' }
        format.json { render :show, status: :created, location: @sub }
      else
        format.html { render :new }
        format.json { render json: @sub.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subs/1
  # PATCH/PUT /subs/1.json
  def update
    @sub = Sub.friendly.find(params[:id])
    respond_to do |format|
      if @sub.update(sub_params)
        format.html { redirect_to @sub, notice: 'Sub was successfully updated.' }
        format.json { render :show, status: :ok, location: @sub }
      else
        format.html { render :edit }
        format.json { render json: @sub.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subs/1
  # DELETE /subs/1.json
  def destroy
    @sub = Sub.friendly.find(params[:id])
    @sub.destroy
    respond_to do |format|
      format.html { redirect_to subs_url, notice: 'Sub was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def must_be_moderator
    sub = Sub.friendly.find(params[:id])
    unless current_user == sub.moderator
      render json: {msg: "You aren't the moderator of this sub."}, status: 401
    end
  end

  def sub_params
    params.require(:sub).permit(:title, :description)
  end
end
