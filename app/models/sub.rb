class Sub < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  validates :title,
            :description,
            :moderator_id,
            presence: true

  belongs_to(
      :moderator,
      foreign_key: :moderator_id,
      primary_key: :id,
      class_name: 'User'
  )

  has_many(
      :posts,
      foreign_key: :sub_id,
      primary_key: :id,
      class_name: 'Post',
      dependent: :destroy
  )

  scope :with_disaster, -> {
    where(:disaster => true)
  }

  scope :without_disaster, -> {
    where(:disaster => false)
  }

end
